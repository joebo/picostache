# picostache.l - Logic-less {{mustache}} templates with PicoLisp

> Sometimes the smallest mustache makes the biggest statement

[picostache](https://bitbucket.org/joebo/picostache/) is an implementation of the [mustache](http://mustache.github.com/) template system in PicoLisp.

[Mustache](http://mustache.github.com/) is a logic-less template syntax. It can be used for HTML, config files, source code - anything. It works by expanding tags in a template using values provided in a hash or object.

We call it "logic-less" because there are no if statements, else clauses, or for loops. Instead 

there are only tags. Some tags are replaced with a value, some nothing, and others a series of values.

For a language-agnostic overview of mustache's template syntax, see the `mustache(5)` [manpage](http://mustache.github.com/mustache.5.html).

## Usage

Below is quick example how to use picostache.:

    (load "picostache.l")
    (push 'Model (cons "name" "joe"))
    (prinl (renderHtml Model "{{name}}"))

     > joe


## Templates

A [mustache](http://mustache.github.com/) template is a string that contains any number of mustache tags. Tags are indicated by the double mustaches that surround them. `{{person}}` is a tag, as is `{{#person}}`. In both examples we refer to `person` as the tag's key.

There are several types of tags available in picostache.

### Variables

The most basic tag type is a simple variable. A `{{name}}` tag renders the value of the `name` key in the current context. If there is no such key, nothing is rendered.

All variables are *NOT* HTML-escaped by default. 

View:

    (push 'Model (cons "name" "Joe"))
    (push 'Model (cons "street" "123 Main St"))

Template:

    * {{name}}
    * {{street}}

Output:

     * Joe
     * 123 Main St

Rendering:

    (prinl (renderHtml Model "* {{name}}^J* {{street}}"))

Or

    (prinl (renderHtml Model "* {{name}}
     * {{street}}"))


### Sections

Sections render blocks of text one or more times, depending on the value of the key in the current context.

A section begins with a pound and ends with a slash. That is, `{{#person}}` begins a `person` section, while `{{/person}}` ends it. The text between the two tags is referred to as that section's "block".

The behavior of the section is determined by the value of the key.

#### False Values or Empty Lists

If the `person` key does not exist, or exists and has a value of `NIL`, `undefined` is an empty list, the block will not be rendered.

View:

    (push 'Model (cons "name"))

Template:

    Shown. {{#name}}never shown{{/name}}

Output:

    Shown.

#### Non-Empty Lists

If the `person` key exists and is not `NIL`, `undefined`, and is not an empty list the 

block will be rendered one or more times.

When the value is a list, the block is rendered once for each item in the list. The context of the 

block is set to the current item in the list for each iteration. In this way we can loop over 

collections.

View:

    (push 'Model (cons "todos" '(((todo . "sleep")) ((todo . "eat"))))) 

Template:

    (setq View "{{#todos}}<b>{{todo}}</b>^J{{/#todos}}")

Output:

    <b>sleep</b>
    <b>eat</b>

When looping over an array of strings, a `.` can be used to refer to the current item in the list.

View:

    (push 'Model '(musketeers . ("Athos" "Aramis" "Porthos" "D'Artagnan")))

Template:

    (setq View "{{#musketeers}}* {{.}}^J{{/musketeers}}")

Output:

    * Athos
    * Aramis
    * Porthos
    * D'Artagnan

#### Functions

If the value of a section variable is a function, it will be called in the context of the current 

item in the list on each iteration.

View:

    (push 'Model '(beatles . (((first . "John") (last . "Lennon")) ((first . "Paul") (last . "McCartney")))))

    (push 'Model (cons "name" '((X) (pack (cdr (assoc 'first X)) " " (cdr (assoc 'last X))))))

Template:

    (setq View "{{#beatles}} * {{name}}^J{{/beatles}}")

Output:

    * John Lennon
    * Paul McCartney



If the value of a section key is a function, it is called with the section's rendered block of text, as its first argument. 

View:

    (push 'Model (cons "name" "Joe"))
    (push 'Model (cons "bold" '((X) (pack "<b>" X "</b>"))))

Template:

    (setq Template "{{#bold}}Hi {{name}}.{{/bold}}")

Output:

    <b>Hi Joe.</b>

### Inverted Sections

An inverted section opens with `{{~section}}` instead of `{{#section}}`. The block of an inverted section is rendered only if the value of that section's tag is `NIL`, `undefined`, or an empty list. **NOTE** This is different than standard mustache due to the way picolisp handles carets.

View:

     #optional, can omit this altogether
     (push 'Model '(todos . NIL))

Template:

     (setq Template "{{#todos}}{{.}}{{/todos}}{{~todos}}no todos!{{/todos}}")

Output:

    no todos!

